//querySelector() method return the first element that matches a selectors


const txtFirstName = document.querySelector('#txt-first-name');
const spanFullName = document.querySelector('#span-full-name');

//addEventListener() - method attaches an event handler to an element
//keyup - Akeyboard key is release after being pushed
txtFirstName.addEventListener('keyup', (event) => {
    spanFullName.innerHTML = txtFirstName.value;
});

/**
 *  What is event target value ?
 * - event.target - gives you the element that triggered the events
 * - event.target.value - return the element where the event occured 
 */

txtFirstName.addEventListener('keyup', (event) => {
    // the target property is read-only
    console.log(event.target);
    console.log(event.target.value);
})
